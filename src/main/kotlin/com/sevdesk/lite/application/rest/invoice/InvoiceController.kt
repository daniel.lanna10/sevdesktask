package com.sevdesk.lite.application.rest.invoice

import com.sevdesk.lite.application.rest.invoice.model.InvoiceRest
import com.sevdesk.lite.application.rest.invoice.model.toDomain
import com.sevdesk.lite.application.rest.invoice.model.toInvoiceRest
import com.sevdesk.lite.application.rest.invoice.model.toInvoiceRestList
import com.sevdesk.lite.domain.invoice.service.InvoiceService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/invoices")
@Validated
class InvoiceController(
    private val invoiceService: InvoiceService
) {

    @GetMapping
    fun getAllInvoices(
        page: Pageable = Pageable.unpaged()
    ): ResponseEntity<Page<InvoiceRest>> = ResponseEntity.ok(invoiceService.getAllInvoices(page).toInvoiceRestList())

    @GetMapping("/{id}")
    fun getInvoice(
        @PathVariable("id") id: Long
    ): ResponseEntity<InvoiceRest> = ResponseEntity.ok(invoiceService.getInvoice(id).toInvoiceRest())

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun addInvoice(
        @RequestBody invoice: InvoiceRest
    ): ResponseEntity<InvoiceRest> = ResponseEntity.ok(invoiceService.saveInvoice(invoice.toDomain()).toInvoiceRest())
}
