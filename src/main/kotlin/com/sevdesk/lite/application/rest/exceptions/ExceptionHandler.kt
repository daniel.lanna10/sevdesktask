package com.sevdesk.lite.application.rest.exceptions

import com.sevdesk.lite.domain.invoice.model.exceptions.AbstractDomainException
import com.sevdesk.lite.domain.invoice.model.exceptions.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
internal class ExceptionHandler {

    data class ProblemResponse(
        val message: String,
        val errorCode: String,
    )

    @ExceptionHandler
    fun handle(exception: AbstractDomainException): ResponseEntity<ProblemResponse> =
        ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
            ProblemResponse(
                message = exception.message,
                errorCode = exception.errorCode.code
            )
        )

    @ExceptionHandler
    fun handle(exception: NotFoundException): ResponseEntity<ProblemResponse> =
        ResponseEntity.status(HttpStatus.NOT_FOUND).body(
            ProblemResponse(
                message = exception.message,
                errorCode = exception.errorCode.code
            )
        )
}