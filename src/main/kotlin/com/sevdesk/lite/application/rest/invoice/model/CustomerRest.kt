package com.sevdesk.lite.application.rest.invoice.model

import com.sevdesk.lite.domain.invoice.model.Customer

data class CustomerRest(
    val id: Long?,
    val givenname: String?,
    val surname: String?,
)

fun Customer.toInvoiceRest() = CustomerRest(
    id = id,
    givenname = givenname,
    surname = surname,
)

fun CustomerRest.toDomain() = Customer(
    id = id,
    givenname = givenname,
    surname = surname,
)