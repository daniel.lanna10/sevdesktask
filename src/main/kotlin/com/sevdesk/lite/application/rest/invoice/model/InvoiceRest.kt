package com.sevdesk.lite.application.rest.invoice.model

import com.sevdesk.lite.domain.invoice.model.Invoice
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import org.springframework.data.domain.Page

data class InvoiceRest(
    val id: Long,
    val status: String,
    val creationDate: OffsetDateTime?,
    val dueDate: LocalDate,
    val invoiceNumber: String,
    val quantity: BigDecimal,
    val priceNet: BigDecimal,
    val priceGross: BigDecimal,
    val customer: CustomerRest,
)

fun Page<Invoice>.toInvoiceRestList() = this.map { it.toInvoiceRest() }

fun Invoice.toInvoiceRest() = InvoiceRest(
    id = id,
    status = status,
    creationDate = creationDate,
    dueDate = dueDate,
    invoiceNumber = invoiceNumber,
    quantity = quantity,
    priceNet = priceNet,
    priceGross = priceGross,
    customer = customer.toInvoiceRest(),
)

fun InvoiceRest.toDomain() = Invoice(
    id = id,
    status = status,
    creationDate = creationDate,
    dueDate = dueDate,
    invoiceNumber = invoiceNumber,
    quantity = quantity,
    priceNet = priceNet,
    priceGross = priceGross,
    customer = customer.toDomain(),
)

