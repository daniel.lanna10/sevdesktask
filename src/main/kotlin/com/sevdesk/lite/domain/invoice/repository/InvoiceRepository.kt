package com.sevdesk.lite.domain.invoice.repository

import com.sevdesk.lite.domain.invoice.model.Invoice
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface InvoiceRepository {
    fun save(invoice: Invoice): Invoice
    fun findById(id: Long): Invoice
    fun findAll(page: Pageable): Page<Invoice>
}
