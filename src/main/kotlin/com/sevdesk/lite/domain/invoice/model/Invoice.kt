package com.sevdesk.lite.domain.invoice.model

import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime

data class Invoice(
    val id: Long,
    val status: String,
    val creationDate: OffsetDateTime?,
    val dueDate: LocalDate,
    val invoiceNumber: String,
    val quantity: BigDecimal,
    val priceNet: BigDecimal,
    val priceGross: BigDecimal,
    val customer: Customer,
)
