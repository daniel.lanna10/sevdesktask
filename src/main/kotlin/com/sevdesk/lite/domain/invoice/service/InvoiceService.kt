package com.sevdesk.lite.domain.invoice.service

import com.sevdesk.lite.domain.invoice.model.Invoice
import com.sevdesk.lite.domain.invoice.model.exceptions.DueDateInThePastException
import com.sevdesk.lite.domain.invoice.model.exceptions.NegativePriceException
import com.sevdesk.lite.domain.invoice.repository.InvoiceRepository
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

class InvoiceService(
    private val invoiceRepository: InvoiceRepository
) {

    fun getAllInvoices(page: Pageable): Page<Invoice> {
        return invoiceRepository.findAll(page)
    }

    fun getInvoice(id: Long): Invoice {
        return invoiceRepository.findById(id)
    }

    fun saveInvoice(invoice: Invoice): Invoice {
        validateInvoice(invoice)
        return invoiceRepository.save(invoice.copy(creationDate = OffsetDateTime.now()))
    }

    private fun validateInvoice(invoice: Invoice) {
        if (invoice.priceNet < BigDecimal.ZERO || invoice.priceGross < BigDecimal.ZERO) {
            throw NegativePriceException(invoiceId = invoice.id, invoiceNumber = invoice.invoiceNumber)
        }
        if (invoice.dueDate.isBefore(LocalDate.now())) {
            throw DueDateInThePastException(invoiceId = invoice.id, invoiceNumber = invoice.invoiceNumber)
        }
    }
}
