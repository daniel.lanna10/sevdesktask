package com.sevdesk.lite.domain.invoice.model

data class Customer(
    val id: Long?,
    val givenname: String?,
    val surname: String?,
)
