package com.sevdesk.lite.domain.invoice.model.exceptions

sealed class AbstractDomainException(
    override val message: String,
    val errorCode: InvoiceExceptionErrorCode,
) : RuntimeException(message)

class NegativePriceException(
    val invoiceId: Long,
    val invoiceNumber: String,
) : AbstractDomainException(
    message = "Invoice with id=$invoiceId and number=$invoiceNumber has negative price.",
    errorCode = InvoiceExceptionErrorCode.NEGATIVE_PRICE,
)

class DueDateInThePastException(
    invoiceId: Long,
    invoiceNumber: String,
) : AbstractDomainException(
    message = "Invoice with id=$invoiceId and number=$invoiceNumber has due date in the past.",
    errorCode = InvoiceExceptionErrorCode.INVALID_DUE_DATE,
)

sealed class NotFoundException(
    message: String,
) : AbstractDomainException(
    message = message,
    errorCode = InvoiceExceptionErrorCode.NOT_FOUND
)

class InvoiceNotFoundException(
    invoiceId: Long,
) : NotFoundException(
    message = "Invoice with id=$invoiceId has not been found.",
)
