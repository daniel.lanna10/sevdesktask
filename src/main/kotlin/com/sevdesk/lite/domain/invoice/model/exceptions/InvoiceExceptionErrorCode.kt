package com.sevdesk.lite.domain.invoice.model.exceptions

enum class InvoiceExceptionErrorCode(
    val code: String,
) {
    NEGATIVE_PRICE("I01"),
    INVALID_DUE_DATE("I02"),
    NOT_FOUND("I03"),
}
