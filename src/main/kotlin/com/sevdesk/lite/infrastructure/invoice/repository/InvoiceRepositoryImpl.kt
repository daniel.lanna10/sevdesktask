package com.sevdesk.lite.infrastructure.invoice.repository

import com.sevdesk.lite.domain.invoice.model.Invoice
import com.sevdesk.lite.domain.invoice.model.exceptions.InvoiceNotFoundException
import com.sevdesk.lite.domain.invoice.repository.InvoiceRepository
import com.sevdesk.lite.infrastructure.invoice.repository.model.InvoiceEntity
import com.sevdesk.lite.infrastructure.invoice.repository.model.toDomain
import com.sevdesk.lite.infrastructure.invoice.repository.model.toEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

class InvoiceRepositoryImpl(
    private val repository: SpringDataInvoiceRepository,
) : InvoiceRepository {

    override fun save(invoice: Invoice): Invoice {
        return repository.save(invoice.toEntity()).toDomain()
    }

    override fun findById(id: Long): Invoice {
        return repository.findById(id).orElseThrow { InvoiceNotFoundException(invoiceId = id) }.toDomain()
    }

    override fun findAll(page: Pageable): Page<Invoice> {
        return repository.findAll(page).map(InvoiceEntity::toDomain)
    }
}
