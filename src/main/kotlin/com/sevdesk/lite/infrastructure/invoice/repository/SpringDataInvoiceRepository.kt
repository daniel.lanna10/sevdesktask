package com.sevdesk.lite.infrastructure.invoice.repository

import com.sevdesk.lite.infrastructure.invoice.repository.model.InvoiceEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface SpringDataInvoiceRepository: PagingAndSortingRepository<InvoiceEntity, Long>