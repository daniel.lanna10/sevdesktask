package com.sevdesk.lite.infrastructure.invoice.repository.model

import com.sevdesk.lite.domain.invoice.model.Customer
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "CUSTOMERS")
data class CustomerEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null,

    @Column(name = "givenname")
    var givenname: String? = null,

    @Column(name = "surname")
    var surname: String? = null,
)

fun Customer.toEntity() = CustomerEntity(
    id = id,
    givenname = givenname,
    surname = surname,
)

fun CustomerEntity.toDomain() = Customer(
    id = id,
    givenname = givenname,
    surname = surname,
)