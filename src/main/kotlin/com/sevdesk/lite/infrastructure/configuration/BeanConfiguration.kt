package com.sevdesk.lite.infrastructure.configuration

import com.sevdesk.lite.LiteApplication
import com.sevdesk.lite.domain.invoice.repository.InvoiceRepository
import com.sevdesk.lite.domain.invoice.service.InvoiceService
import com.sevdesk.lite.infrastructure.invoice.repository.InvoiceRepositoryImpl
import com.sevdesk.lite.infrastructure.invoice.repository.SpringDataInvoiceRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpMethod
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@ComponentScan(basePackageClasses = [LiteApplication::class])
@Profile(SpringProfiles.NOT_TEST)
class BeanConfiguration {

    @Bean
    fun invoiceRepository(repository: SpringDataInvoiceRepository) =
        InvoiceRepositoryImpl(repository = repository)

    @Bean
    fun invoiceService(repository: InvoiceRepository) =
        InvoiceService(invoiceRepository = repository)

    @Bean
    fun corsConfigurer(): WebMvcConfigurer? {
        return object : WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry) {
                registry.addMapping("/**")
                    .allowedOrigins("http://localhost:5173")
                    .allowedMethods(
                        HttpMethod.HEAD.name,
                        HttpMethod.GET.name,
                        HttpMethod.POST.name,
                        HttpMethod.PUT.name,
                        HttpMethod.DELETE.name,
                        HttpMethod.PATCH.name
                    )
            }
        }
    }
}

object SpringProfiles {
    const val TEST = "test"
    const val NOT_TEST = "!$TEST"
}