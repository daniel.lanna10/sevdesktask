package com.sevdesk.lite.infrastructure.invoice.repository.model

import com.sevdesk.lite.domain.invoice.model.Customer
import com.sevdesk.lite.domain.invoice.model.Invoice
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class MappingEntityTest {

    private val creationDate = OffsetDateTime.now()
    private val invoiceDomain = Invoice(
        id = 1L,
        status = "OPEN",
        creationDate = creationDate,
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = Customer(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    private val invoiceEntity = InvoiceEntity(
        id = 1L,
        status = "OPEN",
        creationDate = creationDate,
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = CustomerEntity(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    @Nested
    inner class ToEntity {

        @Test
        fun `should convert the invoice to database entity successfully`() {
            // when
            val result = invoiceDomain.toEntity()

            // then
            assertThat(result).isEqualTo(invoiceEntity)
        }
    }

    @Nested
    inner class ToDomain {

        @Test
        fun `should convert the database invoice to domain representation successfully`() {
            // when
            val result = invoiceEntity.toDomain()

            // then
            assertThat(result).isEqualTo(invoiceDomain)
        }
    }
}
