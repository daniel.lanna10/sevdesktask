package com.sevdesk.lite.infrastructure.invoice.repository

import com.sevdesk.lite.domain.invoice.model.exceptions.InvoiceNotFoundException
import com.sevdesk.lite.infrastructure.invoice.repository.model.CustomerEntity
import com.sevdesk.lite.infrastructure.invoice.repository.model.InvoiceEntity
import com.sevdesk.lite.infrastructure.invoice.repository.model.toDomain
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import java.util.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

class InvoiceRepositoryImplTest {

    private val repository: SpringDataInvoiceRepository = mockk()
    private val invoiceRepository = InvoiceRepositoryImpl(repository = repository)

    @BeforeEach
    fun setUp() {
        clearAllMocks()
    }

    private val invoiceId = 1L
    private val invoiceEntity = InvoiceEntity(
        id = invoiceId,
        status = "OPEN",
        creationDate = OffsetDateTime.now(),
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = CustomerEntity(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    private val invoice = invoiceEntity.toDomain()

    @Nested
    inner class FindAllInvoices {

        @Test
        fun `should find all invoices`() {
            // given
            val invoices = listOf(invoiceEntity)
            every { repository.findAll(Pageable.unpaged()) } returns PageImpl(invoices, Pageable.unpaged(), 1)

            // when
            val result = invoiceRepository.findAll(Pageable.unpaged())

            // then
            assertThat(result.content).isNotEmpty
            assertThat(result.content).isEqualTo(invoices.map { it.toDomain() })
        }

        @Test
        fun `should return empty when there are not invoices`() {
            // given
            every { repository.findAll(Pageable.unpaged()) } returns Page.empty()

            // when
            val result = invoiceRepository.findAll(Pageable.unpaged())

            // then
            assertThat(result.content).isEmpty()
        }
    }

    @Nested
    inner class FindInvoiceById {

        @Test
        fun `should find one invoice when matches the id`() {
            // given
            every { repository.findById(invoiceId) } returns Optional.of(invoiceEntity)

            // when
            val result = invoiceRepository.findById(id = invoiceId)

            // then
            assertThat(result).isNotNull
            assertThat(result).isEqualTo(invoice)
        }

        @Test
        fun `should not find a invoice when the id does not match`() {
            // given
            every { repository.findById(invoiceId) } returns Optional.empty()

            // when & then
            assertThrows<InvoiceNotFoundException> {
                invoiceRepository.findById(id = invoiceId)
            }
        }
    }

    @Nested
    inner class SaveInvoice {

        @Test
        fun `should save an invoice`() {
            // given
            val savedInvoice = invoiceEntity.copy(creationDate = OffsetDateTime.now())
            every { repository.save(invoiceEntity) } returns savedInvoice

            // when
            val result = invoiceRepository.save(invoice)

            // then
            assertThat(result).isNotNull
            assertThat(result).isEqualTo(savedInvoice.toDomain())
        }
    }
}