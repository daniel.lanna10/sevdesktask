package com.sevdesk.lite.domain.invoice.service

import com.sevdesk.lite.domain.invoice.model.Customer
import com.sevdesk.lite.domain.invoice.model.Invoice
import com.sevdesk.lite.domain.invoice.model.exceptions.DueDateInThePastException
import com.sevdesk.lite.domain.invoice.model.exceptions.InvoiceNotFoundException
import com.sevdesk.lite.domain.invoice.model.exceptions.NegativePriceException
import com.sevdesk.lite.domain.invoice.repository.InvoiceRepository
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

class InvoiceServiceTest {

    private val invoiceRepository: InvoiceRepository = mockk()
    private val invoiceService = InvoiceService(invoiceRepository = invoiceRepository)

    @BeforeEach
    fun setUp() {
        clearAllMocks()
    }

    private val invoiceId = 1L
    private val invoice = Invoice(
        id = invoiceId,
        status = "OPEN",
        creationDate = OffsetDateTime.now(),
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = Customer(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    @Nested
    inner class GetAllInvoices {

        @Test
        fun `should find all invoices`() {
            // given
            val invoices = listOf(invoice)
            every { invoiceRepository.findAll(Pageable.unpaged()) } returns
                    PageImpl(invoices, Pageable.unpaged(), 1)

            // when
            val result = invoiceService.getAllInvoices(Pageable.unpaged())

            // then
            assertThat(result.content).isNotEmpty
            assertThat(result.content).isEqualTo(invoices)
        }

        @Test
        fun `should return empty when there are not invoices`() {
            // given
            every { invoiceRepository.findAll(Pageable.unpaged()) } returns Page.empty()

            // when
            val result = invoiceService.getAllInvoices(Pageable.unpaged())

            // then
            assertThat(result).isEmpty()
        }
    }

    @Nested
    inner class GetInvoice {

        @Test
        fun `should find one invoice when matches the id`() {
            // given
            every { invoiceRepository.findById(invoiceId) } returns invoice

            // when
            val result = invoiceService.getInvoice(id = invoiceId)

            // then
            assertThat(result).isNotNull
            assertThat(result).isEqualTo(invoice)
        }

        @Test
        fun `should not find a invoice when the id does not match`() {
            // given
            every { invoiceRepository.findById(invoiceId) } throws InvoiceNotFoundException(invoiceId = invoiceId)

            // when & then
            assertThrows<InvoiceNotFoundException> {
                invoiceService.getInvoice(id = invoiceId)
            }
        }
    }

    @Nested
    inner class SaveInvoice {

        @Test
        fun `should save an invoice`() {
            // given
            val savedInvoice = invoice.copy(creationDate = OffsetDateTime.now())
            every { invoiceRepository.save(any(Invoice::class)) } returns savedInvoice

            // when
            val result = invoiceService.saveInvoice(invoice)

            // then
            assertThat(result).isNotNull
            assertThat(result).isEqualTo(savedInvoice)
        }

        @Test
        fun `should throw an exception when price gross is negative`() {
            // given
            val invoiceToBeSaved = invoice.copy(priceGross = -BigDecimal.TEN)

            // when & then
            assertThrows<NegativePriceException> {
                invoiceService.saveInvoice(invoiceToBeSaved)
            }
        }

        @Test
        fun `should throw an exception when price net is negative`() {
            // given
            val invoiceToBeSaved = invoice.copy(priceNet = -BigDecimal.TEN)

            // when & then
            assertThrows<NegativePriceException> {
                invoiceService.saveInvoice(invoiceToBeSaved)
            }
        }

        @Test
        fun `should throw an exception when due date is in the past`() {
            // given
            val invoiceToBeSaved = invoice.copy(dueDate = LocalDate.now().minusDays(1))

            // when & then
            assertThrows<DueDateInThePastException> {
                invoiceService.saveInvoice(invoiceToBeSaved)
            }
        }
    }
}