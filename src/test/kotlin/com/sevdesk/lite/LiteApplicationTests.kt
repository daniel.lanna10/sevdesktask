package com.sevdesk.lite

import com.sevdesk.lite.application.rest.invoice.InvoiceController
import com.sevdesk.lite.domain.invoice.repository.InvoiceRepository
import com.sevdesk.lite.domain.invoice.service.InvoiceService
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class LiteApplicationTests {

    @Autowired
    lateinit var invoiceController: InvoiceController

    @Autowired
    lateinit var invoiceService: InvoiceService

    @Autowired
    lateinit var invoiceRepository: InvoiceRepository

    @Test
    fun contextLoads() {
        assertThat(invoiceController).isNotNull
        assertThat(invoiceService).isNotNull
        assertThat(invoiceRepository).isNotNull
    }
}
