package com.sevdesk.lite.integrationtest

import com.sevdesk.lite.application.rest.invoice.InvoiceController
import com.sevdesk.lite.application.rest.invoice.model.toDomain
import com.sevdesk.lite.application.rest.invoice.model.toInvoiceRest
import com.sevdesk.lite.domain.invoice.model.Customer
import com.sevdesk.lite.domain.invoice.model.Invoice
import com.sevdesk.lite.domain.invoice.model.exceptions.InvoiceNotFoundException
import com.sevdesk.lite.infrastructure.invoice.repository.SpringDataInvoiceRepository
import com.sevdesk.lite.infrastructure.invoice.repository.model.toDomain
import com.sevdesk.lite.infrastructure.invoice.repository.model.toEntity
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus

@SpringBootTest
class InvoiceIntegrationTest {

    @Autowired
    lateinit var invoiceController: InvoiceController

    @Autowired
    lateinit var repository: SpringDataInvoiceRepository

    @BeforeEach
    fun setUp() {
        repository.deleteAll()
    }

    private val invoice = Invoice(
        id = 1L,
        status = "OPEN",
        creationDate = OffsetDateTime.now(),
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.valueOf(1.0),
        priceNet = BigDecimal.valueOf(1.0),
        priceGross = BigDecimal.valueOf(1.0),
        customer = Customer(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    @Nested
    inner class GetAllInvoices {

        @Test
        fun `should find all invoices`() {
            // given
            val savedInvoice = repository.save(invoice.toEntity())

            // when
            val result = invoiceController.getAllInvoices()

            // then
            assertThat(result.statusCodeValue).isEqualTo(HttpStatus.OK.value())
            val body = result.body!!
            assertThat(body).hasSize(1)
            assertThat(body.first().toDomain()).isEqualTo(savedInvoice.toDomain())
        }

        @Test
        fun `should return empty when there are not invoices`() {
            // given
            repository.deleteAll()

            // when
            val result = invoiceController.getAllInvoices()

            // then
            assertThat(result.statusCodeValue).isEqualTo(HttpStatus.OK.value())
            assertThat(result.body).isEmpty()
        }
    }

    @Nested
    inner class GetInvoice {

        @Test
        fun `should find one invoice when matches the id`() {
            // given
            val savedInvoice = repository.save(invoice.toEntity()).toDomain()

            // when
            val result = invoiceController.getInvoice(id = savedInvoice.id)

            // then
            assertThat(result.statusCodeValue).isEqualTo(HttpStatus.OK.value())
            assertThat(result).isNotNull
            assertThat(result.body).isEqualTo(savedInvoice.toInvoiceRest())
        }

        @Test
        fun `should not find a invoice when the id does not match`() {
            // given
            repository.deleteAll()

            // when
            assertThrows<InvoiceNotFoundException> {
                invoiceController.getInvoice(id = 1L)
            }
        }
    }

    @Nested
    inner class AddInvoice {

        @Test
        fun `should save an invoice`() {
            // when
            val addInvoiceResult = invoiceController.addInvoice(invoice.toInvoiceRest())
            assertThat(addInvoiceResult.statusCodeValue).isEqualTo(HttpStatus.OK.value())
            assertThat(addInvoiceResult.body!!).isNotNull

            // then
            val result = invoiceController.getInvoice(addInvoiceResult.body!!.id)
            assertThat(addInvoiceResult.statusCodeValue).isEqualTo(HttpStatus.OK.value())
            assertThat(result).isNotNull
            assertThat(result.body!!).isEqualTo(addInvoiceResult.body!!)
        }
    }
}