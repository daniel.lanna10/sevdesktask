package com.sevdesk.lite.application.rest.invoice.model

import com.sevdesk.lite.domain.invoice.model.Customer
import com.sevdesk.lite.domain.invoice.model.Invoice
import java.math.BigDecimal
import java.time.LocalDate
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class MappingRestTest {

    private val invoiceDomain = Invoice(
        id = 1L,
        status = "OPEN",
        creationDate = null,
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = Customer(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    private val invoiceRest = InvoiceRest(
        id = 1L,
        status = "OPEN",
        creationDate = null,
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = CustomerRest(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )

    @Nested
    inner class ToRest {

        @Test
        fun `should convert the invoice to rest representation successfully`() {
            // when
            val result = invoiceDomain.toInvoiceRest()

            // then
            assertThat(result).isEqualTo(invoiceRest)
        }
    }

    @Nested
    inner class ToDomain {

        @Test
        fun `should convert the invoice rest representation to domain representation successfully`() {
            // when
            val result = invoiceRest.toDomain()

            // then
            assertThat(result).isEqualTo(invoiceDomain)
        }
    }
}
