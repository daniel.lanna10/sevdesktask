package com.sevdesk.lite.application.rest.invoice

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.sevdesk.lite.PageImplDeserializer
import com.sevdesk.lite.application.rest.invoice.model.CustomerRest
import com.sevdesk.lite.application.rest.invoice.model.InvoiceRest
import com.sevdesk.lite.application.rest.invoice.model.toDomain
import com.sevdesk.lite.application.rest.invoice.model.toInvoiceRest
import com.sevdesk.lite.domain.invoice.model.exceptions.DueDateInThePastException
import com.sevdesk.lite.domain.invoice.model.exceptions.InvoiceNotFoundException
import com.sevdesk.lite.domain.invoice.model.exceptions.NegativePriceException
import com.sevdesk.lite.domain.invoice.service.InvoiceService
import com.sevdesk.lite.infrastructure.configuration.SpringProfiles
import io.mockk.every
import io.mockk.mockk
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(SpringProfiles.TEST)
@Import(ControllerTestConfiguration::class)
internal class InvoiceControllerTest {

    @Autowired
    private lateinit var invoiceService: InvoiceService

    @Autowired
    private lateinit var mockMvc: MockMvc

    private val objectMapper = jacksonObjectMapper()
        .findAndRegisterModules()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)

    private val invoiceId = 1L
    private val invoiceRest = InvoiceRest(
        id = invoiceId,
        status = "OPEN",
        creationDate = null,
        dueDate = LocalDate.now(),
        invoiceNumber = "1",
        quantity = BigDecimal.ONE,
        priceNet = BigDecimal.ONE,
        priceGross = BigDecimal.ONE,
        customer = CustomerRest(
            id = 1L,
            givenname = "John",
            surname = "Doe"
        ),
    )
    private val domainInvoices = listOf(invoiceRest.toDomain())

    @Nested
    inner class GetAllInvoices {

        @Test
        fun `should find all invoices`() {
            // given
            val invoices = listOf(invoiceRest)
            every { invoiceService.getAllInvoices(any(Pageable::class)) } returns
                    PageImpl(domainInvoices, Pageable.unpaged(), 1)

            // when
            val result = mockMvc.get("/invoices").andReturn()

            // then
            val response =
                objectMapper.readValue(
                    result.response.contentAsString,
                    object : TypeReference<PageImplDeserializer<InvoiceRest>>() {}
                )
            assertThat(result.response.status).isEqualTo(HttpStatus.OK.value())
            assertThat(response.content).isNotNull
            assertThat(response.content).hasSize(1)
            assertThat(response.content).isEqualTo(invoices)
        }

        @Test
        fun `should return empty when there are not invoices`() {
            // given
            every { invoiceService.getAllInvoices(any(Pageable::class)) } returns Page.empty()

            // when
            val result = mockMvc.get("/invoices").andReturn()

            // then
            val response =
                objectMapper.readValue(
                    result.response.contentAsString,
                    object : TypeReference<PageImplDeserializer<InvoiceRest>>() {}
                )
            assertThat(result.response.status).isEqualTo(HttpStatus.OK.value())
            assertThat(response.content).isEmpty()
        }
    }

    @Nested
    inner class GetInvoice {

        @Test
        fun `should find one invoice when matches the id`() {
            // given
            every { invoiceService.getInvoice(invoiceId) } returns invoiceRest.toDomain()

            // when
            val result = mockMvc.get("/invoices/$invoiceId").andReturn()

            // then
            val content =
                objectMapper.readValue(result.response.contentAsString, InvoiceRest::class.java)
            assertThat(result.response.status).isEqualTo(HttpStatus.OK.value())
            assertThat(content).isNotNull
            assertThat(content).isEqualTo(invoiceRest)
        }

        @Test
        fun `should not find a invoice when the id does not match`() {
            // given
            every { invoiceService.getInvoice(invoiceId) } throws InvoiceNotFoundException(invoiceId = invoiceId)

            // when
            val result = mockMvc.get("/invoices/$invoiceId").andReturn()

            // then
            assertThat(result.response.status).isEqualTo(HttpStatus.NOT_FOUND.value())
            assertThat(result.response.contentAsString).contains("""errorCode":"I03""")
        }
    }

    @Nested
    inner class AddInvoice {

        @Test
        fun `should save an invoice`() {
            // given
            val invoiceToBeSaved = invoiceRest
            val savedInvoice = invoiceToBeSaved.toDomain().copy(creationDate = OffsetDateTime.now())
            every { invoiceService.saveInvoice(invoiceToBeSaved.toDomain()) } returns savedInvoice

            // when
            val result = mockMvc.post("/invoices") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(invoiceToBeSaved)
            }.andReturn()

            // then
            val content =
                objectMapper.readValue(result.response.contentAsString, InvoiceRest::class.java)
            assertThat(result.response.status).isEqualTo(HttpStatus.OK.value())
            assertThat(content).isNotNull
            assertThat(content).isEqualTo(savedInvoice.toInvoiceRest())
        }

        @Test
        fun `should return bad request when the gross price is negative`() {
            // given
            val invoiceToBeSaved = invoiceRest.copy(priceGross = -BigDecimal.TEN)
            every { invoiceService.saveInvoice(invoiceToBeSaved.toDomain()) } throws NegativePriceException(
                invoiceId = invoiceId,
                invoiceNumber = invoiceRest.invoiceNumber
            )

            // when
            val result = mockMvc.post("/invoices") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(invoiceToBeSaved)
            }.andReturn()

            // then
            assertThat(result.response.status).isEqualTo(HttpStatus.BAD_REQUEST.value())
            assertThat(result.response.contentAsString).contains("""errorCode":"I01""")
        }

        @Test
        fun `should return bad request when the net price is negative`() {
            // given
            val invoiceToBeSaved = invoiceRest.copy(priceNet = -BigDecimal.TEN)
            every { invoiceService.saveInvoice(invoiceToBeSaved.toDomain()) } throws NegativePriceException(
                invoiceId = invoiceId,
                invoiceNumber = invoiceRest.invoiceNumber
            )

            // when
            val result = mockMvc.post("/invoices") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(invoiceToBeSaved)
            }.andReturn()

            // then
            assertThat(result.response.status).isEqualTo(HttpStatus.BAD_REQUEST.value())
            assertThat(result.response.contentAsString).contains("""errorCode":"I01""")
        }

        @Test
        fun `should return bad request when the due date is in the past`() {
            // given
            val invoiceToBeSaved = invoiceRest.copy(dueDate = LocalDate.now().minusDays(1))
            every { invoiceService.saveInvoice(invoiceToBeSaved.toDomain()) } throws DueDateInThePastException(
                invoiceId = invoiceId,
                invoiceNumber = invoiceRest.invoiceNumber
            )

            // when
            val result = mockMvc.post("/invoices") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(invoiceToBeSaved)
            }.andReturn()

            // then
            assertThat(result.response.status).isEqualTo(HttpStatus.BAD_REQUEST.value())
            assertThat(result.response.contentAsString).contains("""errorCode":"I02""")
        }
    }
}

@TestConfiguration
@Profile(SpringProfiles.TEST)
class ControllerTestConfiguration {

    @Bean
    fun invoiceService(): InvoiceService = mockk()
}
